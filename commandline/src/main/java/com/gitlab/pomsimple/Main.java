package com.gitlab.pomsimple;

import com.gitlab.inject.*;
import com.gitlab.crypto.*;
import com.gitlab.util.ScenarioRunner;

import java.util.Date;
import java.util.Scanner;

public class Main {
	// Add new CLI classes to the list below
	private static final Class<?>[] classesToRun = {
			RsaNoPadding.class,
			TestLdapInjection.class
	};

	public static void main(String[] args) throws Exception {
		String input;
		if(args.length > 0) {
			input = args[0];
		} else {
			Scanner scanner = new Scanner(System.in);

			System.out.print("Enter the class name (or 'all' to run all classes): ");
			input = scanner.nextLine();
		}

		processCommandLine(input);
	}

	private static void processCommandLine(String input) {
		if ("all".equalsIgnoreCase(input)) {
			// Run all classes
			for (Class<?> clazz : classesToRun) {
				System.out.printf("%s: Starting %s...\n", new Date(), clazz.getSimpleName());
				runClass(clazz);
				System.out.printf("%s: Stopped %s...\n", new Date(), clazz.getSimpleName());
			}
		} else if("list".equalsIgnoreCase(input)) {
			for (Class<?> clazz : classesToRun) {
				System.out.println(clazz.getSimpleName());
			}
		}
		else {
			// Run the specified class
			for (Class<?> clazz : classesToRun) {
				if (clazz.getSimpleName().equals(input)) {
					System.out.printf("%s: Starting %s...\n", new Date(), clazz.getSimpleName());
					runClass(clazz);
					System.out.printf("%s: Stopped %s...\n", new Date(), clazz.getSimpleName());
				}
			}
		}
	}

	private static void runClass(Class<?> clazz) {
		try {
			// Create an instance of the class and invoke a method
			Object instance = clazz.getDeclaredConstructor().newInstance();
			if (instance instanceof ScenarioRunner) {
				((ScenarioRunner) instance).run();
			}
		} catch (Exception e) {
			// Handle exceptions (skip the class)
			System.err.println("Exception while running class " + clazz.getSimpleName() + ": " + e.getMessage());
		}
	}
}