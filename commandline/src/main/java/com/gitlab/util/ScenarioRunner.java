package com.gitlab.util;

public interface ScenarioRunner {
    void run() throws Exception;
}
